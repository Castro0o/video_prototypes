#!/bin/sh
# CREATE A MOSAIC OUT OF SEVERAL INPUT VIDEOS
# As described in https://trac.ffmpeg.org/wiki/Create%20a%20mosaic%20out%20of%20several%20input%20videos

ffmpeg -i video_2.mp4 -i video_4.mp4 -i video_3.mp4 -i video_1.mp4  -filter_complex "nullsrc=size=1920x1080 [base];
[0:v] setpts=PTS-STARTPTS, scale=960x540 [0x0];
[1:v] setpts=PTS-STARTPTS, scale=960x540 [0x1];
[2:v] setpts=PTS-STARTPTS, scale=960x540 [1x0];
[3:v] setpts=PTS-STARTPTS, scale=960x540 [1x1];
[base][0x0] overlay=shortest=0:x=0:y=0 [tmp0];
[tmp0][0x1] overlay=shortest=0:x=960:y=0 [tmp1];
[tmp1][1x0] overlay=shortest=0:x=0:y=540 [tmp2];
