#!/bin/sh

# Audio: amix; mix audio with amerge filter
set 'amix=inputs=4:duration=longest:dropout_transition=0'
echo "$1"

ffmpeg -i video_1.mp4 \
       -i video_2.mp4 \
       -i video_3.mp4 \
       -i video_4.mp4 \
       -filter_complex "$1" \
       -c:a libmp3lame \
       tmp_a.mp3

# create mosaic with overlay
set 'nullsrc=size=1280x480 [base];
[0:v] setpts=PTS-STARTPTS, scale=426x240 [upperleft];
[1:v] setpts=PTS-STARTPTS, scale=426x240 [upperright];
[2:v] setpts=PTS-STARTPTS, scale=426x240 [lowerleft];
[3:v] setpts=PTS-STARTPTS, scale=1280x240 [lowerright];
[base][upperleft] overlay=shortest=0:x=0:y=0 [tmp1];
[tmp1][upperright] overlay=shortest=0:x=426:y=0 [tmp2];
[tmp2][lowerleft] overlay=shortest=0:x=852:y=0 [tmp3];
[tmp3][lowerright] overlay=shortest=1:x=0:y=240'
echo "$1"

# if we want to create overlays of the many videos in a dir
# we will need to generate ffmpeg's -filer_complex line syntax for each video:
# - input tagging variables:
#    - input num
#    - scale (division of nullsrc=size)
#    - [tag]
# - overlay inputs
#    - [prev tag][current input tag] (careful of [base])
#    - overlay=
#        - shortest=0 (1 only for the longest video)


# TAGGING INPUTS:
# nullsrc=size=1280x240 [base];
# * nullsrc is background video with name [base]
#
# [0:v] setpts=PTS-STARTPTS, scale=320x240 [upperleft];
# * [0:v] addresses 1st input, [1:v] 2nd input
# * "setpts=PTS-STARTPTS": ensures video input starts at 0
# * scale=320x240: scales video input
# * [name] - a tag
#
# OVERLAY:
# [base][upperleft] overlay=shortest=0:x=0:y=0 [tmp1];
# * [base][upperleft] - start w/ [base] and overlay [upperleft] upon it
# * overlay= - overlay parameters
# * shortest=0 - if 1 overlay video should stop when this input stops (reserve for longest)
# * x,y - position
# * [tmp1] tag, to be used in following overlay. As
# [prev-overlay-tag][input-tag] 

ffmpeg -i video_1.mp4 \
       -i video_2.mp4 \
       -i video_3.mp4 \
       -i video_4.mp4 \
       -filter_complex "$1" -c:v h264 tmp_v.mp4

# merge audio and video
ffmpeg -i tmp_a.mp3 -i tmp_v.mp4 -acodec copy -vcodec copy output.mp4 -y

rm tmp_v.mp4 tmp_a.mp3

# # do not repeat inputs

