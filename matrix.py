#! /usr/bin/env python

import numpy as np
from datetime import datetime
import random, shlex, subprocess, os
from pymediainfo import MediaInfo


lsdir = os.listdir('.')
files = [f for f in lsdir if ".mp4" in f and "-overlay" not in f ] # "and "-overlay" not in f" prevents recursion in video matrix (a matrix within a matrix)!
files_and_dur = [(f,(MediaInfo.parse(f)).tracks[0].duration) for f in files]

files_and_dur_sorted = sorted(files_and_dur, key=lambda x: x[1])
file_w_max_dur, max_dur = files_and_dur_sorted[-1]
#print file_w_max_dur, max_dur
files_len = len(files)
print 'total of', files_len, 'input files', files_and_dur

# handle prime number files_len 
def is_prime(x):
    return divmod(2 ** (x - 1), x)[1] == 1

if is_prime(files_len) is True:
    files=files[:-1] #remove last element, so that files_len is not prime
    files_len = len(files)
    
divisors = [] # append all dividers w/out remainers
n = 1 # divider
for i in files:
    rem = files_len % n #remainer of len(files)/n
    if rem == 0:
        divisors.append(n)
    n+=1
    
divisors = divisors[1:-1] #remove first and last items  
print 'all possible divisors: ', divisors
rows=random.choice(divisors) #random number of collums
collums=files_len / rows
print 'divisors', divisors
print collums, rows
# reshape the files liest onto a grid (matrix)
data = np.array( files )
shape = ( rows, collums ) # (rows x collums) == len(data)
print 'original shape', shape
if shape[0] > shape[1]: # reverse shape to get landscape
    l = list(shape)
    l.reverse()
    shape = tuple(l)
    print 'reveserd shape', shape
matrix = data.reshape( shape )
print matrix

# dimensions: frame, each cell
frame_res={"w":1920, "h":1080}
cell_res={ "h":frame_res["h"]/shape[0],
           "w": frame_res["w"]/shape[1]
       }
print cell_res

def ffmpeg_filter(matrix, resolution):
    # templates
    ffmpeg_background = "nullsrc=size={}x{} [base];".format(resolution['w'], resolution['h'])
    ffmpeg_input_template = "[{N}:v] setpts=PTS-STARTPTS, scale={w}x{h} [{tag}];"
    ffmpeg_tag_template = "[{prev_tag}][{current_input}] overlay=shortest={short}:x={x}:y={y} [{current_tag}];"
    # variables
    cell_n=0
    y=0
    x=0
    current_tag = 'base'
    overlay_inputs = ''
    overlay_tags = ''
    # generate input and tag strings
    for row_pos, row in enumerate(matrix):
        #print row_pos, row, y,x 
        for cell_pos, cell in enumerate(row):
            print 'cell', cell
            input_tag = ('{}x{}'.format(str(y),str(x)))
            prev_tag = current_tag
            current_tag="tmp"+str(cell_n)
            this_input = ffmpeg_input_template.format(N=cell_n ,w=cell_res['w'], h=cell_res['h'], tag=input_tag)
            this_tag = ffmpeg_tag_template.format(prev_tag= prev_tag,
                                                  current_input=input_tag ,
                                                  short = (str(1) if cell == file_w_max_dur else str(0)),
                                                  x = str( int(cell_res['w']) * x ),
                                                  y = str( int(cell_res['h']) * y ),
                                                  current_tag=current_tag
            )
            overlay_inputs += this_input + '\n'        
            overlay_tags += this_tag + '\n'
            cell_n += 1
            x += 1

        x = 0 # reset x
        y += 1
    overlay_filter = ffmpeg_background+'\n'+overlay_inputs + overlay_tags[:-9] #rm from end [tmpN];\n
    return overlay_filter

def ffmpeg_inputs(files):
    inputs = ''
    for f in files:
        inputs += "-i {} ".format(f)
    return inputs

# inputs = ffmpeg_inputs(files)
# overlay_filter = ffmpeg_filter(matrix, frame_res)
# now = (datetime.now().strftime('%y%m%d-%H%M'))
# ffmpeg_command = 'ffmpeg {inputs} -filter_complex "{ofilter}" -c:v libx264 "{outfile}"'.format(inputs=inputs , ofilter=overlay_filter, outfile=now+"-overlay.mp4")
# ffmpeg_command_list = shlex.split(ffmpeg_command)
# print ffmpeg_command
# subprocess.call(ffmpeg_command_list)



