#!/bin/sh
# cut a video into increasingly long video files 
IN=$1

for i in `seq 1 10`;
do dur=`expr 2 \+ $i`; # increase dur at each iteration
   start=`expr $dur \* $i`;
   f=video_$i.mp4;
   echo $start $dur $f;
   ffmpeg -y -ss $start -i $IN -t $dur -c:v copy "$f";
done
